// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: ThreadSafeObjectPool
  Authors: ATLAS ROS group 	
*/

#ifndef THREADSAFEOBJECTPOOL_H
#define THREADSAFEOBJECTPOOL_H

#include <DFThreads/DFFastMutex.h>

template <class T>
class ThreadSafeObjectPool {
 public:
  enum { MIN_SIZE = 2 * sizeof(void *) } ; 
  enum { CLASS_SIZE = sizeof(T) } ;
  static void *allocate() ;
  static void deallocate(void *space) ;
  ~ThreadSafeObjectPool() ;
 private:
  static DFFastMutex * m_allocationMutex ;
  static DFFastMutex * m_deallocationMutex ;
  static const int m_size = ( (int)CLASS_SIZE > (int)MIN_SIZE ? (int)CLASS_SIZE : (int)MIN_SIZE ) ;
  static const int m_poolStep = 128 * m_size ;
  union dataStructure { 
    struct {
      union dataStructure *next ;
      union dataStructure *previous ;
    } linkList ;
    char data[MIN_SIZE] ;
  } ;
  static dataStructure * increasePool() ;
  static dataStructure * volatile m_first;
  static dataStructure * volatile m_last;
};

template <class T>
DFFastMutex * ThreadSafeObjectPool<T>::m_allocationMutex = DFFastMutex::Create() ;

template <class T>
DFFastMutex * ThreadSafeObjectPool<T>::m_deallocationMutex = DFFastMutex::Create() ;

template <class T>
inline ThreadSafeObjectPool<T>::~ThreadSafeObjectPool() {
  m_allocationMutex->destroy() ;
  m_deallocationMutex->destroy() ;
}

template <class T>
typename ThreadSafeObjectPool<T>::dataStructure * volatile ThreadSafeObjectPool<T>::m_last = 0 ;

template <class T>
inline typename ThreadSafeObjectPool<T>::dataStructure * ThreadSafeObjectPool<T>::increasePool() {
  char *space = (char *)malloc(m_poolStep * sizeof(char)) ; 
  dataStructure *last = (dataStructure *)space ;
  (last->linkList).previous = m_last;
  dataStructure * current ;
  for (int i=m_size;i<m_poolStep;i+=m_size) {
    current= (dataStructure *)(space + i) ;
    (current->linkList).previous = last ;
    (last->linkList).next = current;
    last = current ;
  }
  (last->linkList).next = 0 ;
  m_last = last ;
  return (dataStructure *)space ;
}

template <class T>
typename ThreadSafeObjectPool<T>::dataStructure * volatile ThreadSafeObjectPool<T>::m_first = ThreadSafeObjectPool<T>::increasePool() ;

template <class T> 
inline void * ThreadSafeObjectPool<T>::allocate() {
  void * result ;
  m_allocationMutex->lock() ;
  if (m_last==m_first) {
     dataStructure * last_old = m_last ;
     (last_old->linkList).next = increasePool() ;
  }
  result = (void *)m_last ;
  m_last = (m_last->linkList).previous ;
  m_allocationMutex->unlock() ;
  return result ;
}

template <class T> 
inline void ThreadSafeObjectPool<T>::deallocate(void *space) {
  m_deallocationMutex->lock() ;
  (((dataStructure *)space)->linkList).next = m_first ;
  (((dataStructure *)space)->linkList).previous = 0 ;
  (m_first->linkList).previous = (dataStructure *)space ;
  m_first = (dataStructure *)space;
  m_deallocationMutex->unlock() ;
}


#endif //THREADSAFEOBJECTPOOL_H
