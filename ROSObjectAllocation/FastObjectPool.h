// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: FastObjectPool
  Authors: ATLAS ROS group 	
*/

#ifndef FASTOBJECTPOOL_H
#define FASTOBJECTPOOL_H

#include <iostream>
#include <stdlib.h>


template <class T>
class FastObjectPool {
 public:
  enum { SIZE = sizeof(T) } ;
  enum { POOL_STEP = 128 * SIZE } ;
  static void *allocate() ;
  static void deallocate(void *space) ;
  static void print(void) ;
 private:
  union dataStructure { 
    union dataStructure *nextElement ;
    char data[SIZE] ;
  } ;
  static dataStructure * increasePool() ;
  static dataStructure * volatile m_next;
};

template <class T>
inline typename FastObjectPool<T>::dataStructure * FastObjectPool<T>::increasePool() {
  char *space = (char *)malloc(POOL_STEP * sizeof(char)) ; 
  dataStructure *last = (dataStructure *)space ;
  for (int i=SIZE;i<POOL_STEP;i+=SIZE) {
    last->nextElement = (dataStructure *)(space + i) ;
    last = last->nextElement ;
  }
  last->nextElement = 0 ;
  return (dataStructure *)space ;
}

template <class T>
typename FastObjectPool<T>::dataStructure * volatile FastObjectPool<T>::m_next = FastObjectPool<T>::increasePool() ;

template <class T> 
inline void * FastObjectPool<T>::allocate() {
  if (m_next==0) {
    m_next = increasePool() ;
  }
  void * result = (void *)(m_next->data) ;
  m_next = m_next->nextElement ;
  return result ;
}

template <class T> 
inline void FastObjectPool<T>::deallocate(void *space) {
  ((dataStructure *)space)->nextElement = m_next ;
  m_next = (dataStructure *)space;
}

template <class T>
void FastObjectPool<T>::print(void) {

  int no = 0;

  std::cout << " m_next = " << m_next;
  std::cout << " object size = " <<  SIZE << std::endl;
  std::cout << " object addresses: " << std::endl;

  dataStructure * current = m_next;
  while ( current != 0) {
	  std::cout << "  " << current;
    current = current->nextElement;
    no++;
    if ((no % 8) == 0) std::cout << std::endl;
  }

  std::cout << "\n # elements in free list = " << no << std::endl;

}


#endif //FASTOBJECTPOOL_H
