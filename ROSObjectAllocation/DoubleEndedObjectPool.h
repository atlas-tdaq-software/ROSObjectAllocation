// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: DoubleEndedObjectPool
  Authors: ATLAS ROS group 	
*/

#ifndef DOUBLEENDEDOBJECTPOOL_H
#define DOUBLEENDEDOBJECTPOOL_H

#include <stdlib.h>
#include <iostream>

template <class T>
class DoubleEndedObjectPool {
 public:
  enum { MIN_SIZE = 2 * sizeof(void *) } ; 
  enum { CLASS_SIZE = sizeof(T) } ;
  static void *allocate() ;
  static void deallocate(void *space) ;
  static void print(void) ;
  ~DoubleEndedObjectPool() {}
 private:
  static const int m_size = ( (int)CLASS_SIZE > (int)MIN_SIZE ? (int)CLASS_SIZE : (int)MIN_SIZE ) ;
  static const int m_poolStep = 128 * m_size ;
  union dataStructure { 
    struct {
      union dataStructure *next ;
      union dataStructure *previous ;
    } linkList ;
    char data[MIN_SIZE] ;
  } ;
  static dataStructure * increasePool() ;
  static dataStructure * volatile m_first;
  static dataStructure * volatile m_last;
};

template <class T>
typename DoubleEndedObjectPool<T>::dataStructure * volatile DoubleEndedObjectPool<T>::m_last = 0 ;

template <class T>
inline typename DoubleEndedObjectPool<T>::dataStructure * DoubleEndedObjectPool<T>::increasePool() {
  char *space = (char *)malloc(m_poolStep * sizeof(char)) ; 
  dataStructure *last = (dataStructure *)space ;
  (last->linkList).previous = m_last;
  dataStructure * current ;
  for (int i=m_size;i<m_poolStep;i+=m_size) {
    current= (dataStructure *)(space + i) ;
    (current->linkList).previous = last ;
    (last->linkList).next = current;
    last = current ;
  }
  (last->linkList).next = 0 ;
  m_last = last ;
  return (dataStructure *)space ;
}

template <class T>
typename DoubleEndedObjectPool<T>::dataStructure * volatile DoubleEndedObjectPool<T>::m_first = DoubleEndedObjectPool<T>::increasePool() ;

template <class T> 
inline void * DoubleEndedObjectPool<T>::allocate() {
  void * result ;
  if (m_last==m_first) {
     dataStructure * last_old = m_last ;
     (last_old->linkList).next = increasePool() ;
  }
  result = (void *)m_last ;
  m_last = (m_last->linkList).previous ;
  return result ;
}

template <class T> 
inline void DoubleEndedObjectPool<T>::deallocate(void *space) {
  (((dataStructure *)space)->linkList).next = m_first ;
  (((dataStructure *)space)->linkList).previous = 0 ;
  (m_first->linkList).previous = (dataStructure *)space ;
  m_first = (dataStructure *)space;
}

template <class T>
void DoubleEndedObjectPool<T>::print(void) {

  int no = 0;

  std::cout << " m_first = " << m_first;
  std::cout << "  m_last = " << m_last;
  std::cout << "  object size = " <<  CLASS_SIZE << std::endl;
  std::cout << "  object addresses: " << std::endl;

  dataStructure * current = m_first;
  dataStructure * currentSave = 0 ;
  while ( currentSave != m_last) {
	  std::cout << "  " << current;
    currentSave = current;
    current = (current->linkList).next;
    no++;
    if ((no % 8) == 0) std::cout << std::endl;
  }

  std::cout << "\n # elements in free list = " << no << std::endl;

}

#endif //DOUBLEENDEDOBJECTPOOL_H
