/****************************************************************/
/*                                                              */
/*  file testFastObjectPool.cpp                                 */
/*                                                              */
/*  exerciser for the fast Object Pool                          */
/*                                                              */
/*  The program allows to:                                      */
/*  . exercise the methods of the FastObjectPool		*/
/*  . measure the timing performance                            */
/*                                                              */
/*                                                              */
/****************************************************************/

#include <iostream>
#include <iomanip>
#include "ROSGetInput/get_input.h"

#include "ROSObjectAllocation/FastObjectPool.h"

enum {ALLOCATE = 0x01,
      DEALLOCATE,
      PRINTLIST,
      TIMING,
      HELP,
      QUIT = 100
};

// our example class 
class ex_class {
  int a[8];
};

int main() {

  char option;
  int quit_flag = 0;

  int noObjects;
  ex_class * obj_ptr;

  FastObjectPool<ex_class> FastPool;

do {

	std::cout << std::endl << std::endl;
	std::cout << " allocate object    : " << std::dec <<  ALLOCATE << std::endl;
	std::cout << " de-allocate object : " << std::dec <<  DEALLOCATE << std::endl;
	std::cout << " print free list    : " << std::dec <<  PRINTLIST << std::endl;
	std::cout << " TIMING             : " << std::dec <<  TIMING << std::endl;
	std::cout << " HELP               : " << std::dec <<  HELP << std::endl;
	std::cout << " QUIT               : " << std::dec <<  QUIT << std::endl;

	std::cout << " ? : ";

  option = getdec();

  switch(option)   {

  case ALLOCATE :

	  std::cout << " # objects to allocate: ";
    noObjects = getdecd(1);

    for (int i = 0; i < noObjects; i++) {
      obj_ptr = (ex_class *) FastPool.allocate();
      std::cout << " got object " << " #" << i <<  " @ " << obj_ptr << std::endl;
    }

  break;

  case DEALLOCATE :

  std::cout << " object pointer: ";
    obj_ptr = (ex_class*) gethex();
    
    FastPool.deallocate(obj_ptr);

  break;

  case PRINTLIST :

  FastPool.print();

  break;

  case TIMING :

  std::cout << " not implemented" << std::endl;

  break;

  case HELP :

  std::cout << " is self-explanatory .." << std::endl;

  break;

  case QUIT :

    quit_flag = 1;

    break;

  }

} while (quit_flag == 0);

}
